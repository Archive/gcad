# Note that this is NOT a relocatable package
%define ver      0.0.1
%define rel      SNAP
%define prefix   /usr/local

Summary: GNOME simple 2D CAD application
Name: gcad
Version: %ver
Release: %rel
Copyright: GPL
Group: X11/Utilities
Source: ftp:// ? /gcad-%{ver}.tar.gz
BuildRoot: /tmp/ee-root
Packager: I�igo Serna <inigo@bipv02.bi.ehu.es>
URL: http://www.gnome.org
Docdir: %{prefix}/doc

%description
gcad is a simple 2D CAD application.

%changelog

%prep
%setup

%build

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{prefix}/bin/*
%{prefix}/share/gnome/help/*
%{prefix}/share/locale/*/*/*
%{prefix}/share/apps/*
