/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>


#define APPNAME PACKAGE" - "VERSION
#define COPYRIGHT_NOTICE "Copyright 1998, under the GNU General Public License."
#define AUTHOR "I�igo Serna <inigo@bipv02.bi.ehu.es>"

#define DRAW_EXT ".gcad"


/*************
 * NEW TYPES *
 *************/

/* drawing struct */
typedef struct {
   gchar    *name;			/* drawing name */
   gchar    *comment;			/* some comments about the drawing */
   gdouble  height, width;		/* logical dimensions, in mms. */
   gboolean polar, ortho, snap, grid;
   gdouble  snap_x, snap_y, snap_ang;	/* snap steps, in mms. */
   gdouble  grid_x, grid_y;		/* grid intervals, in mms. */
   gdouble  org_x1, org_y1;		/* drawing original limits, in mms. */
   gdouble  org_x2, org_y2;
   gdouble  scr_x1, scr_y1;		/* drawing limits, in mms. */
   gdouble  scr_x2, scr_y2;
   gdouble  win_x1, win_y1;		/* window limits, in mms. */
   gdouble  win_x2, win_y2;
   gdouble  p_win_x1, p_win_y1;		/* previous zoom window limits, in mms. */
   gdouble  p_win_x2, p_win_y2;
   gdouble  zoom;   			/* zoom value, in % */
   guint    layers_num;			/* number of layers */
   guint    layer_i;			/* current layer */
   GList    *layers;			/* layers */
} draw_t;   

/* layer struct */
typedef struct {
   gchar    *name;		/* name of layer */
   gint     r, g, b;		/* color */
   gint     line_type;		/* line type */
   gint     line_anchor;	/* line width */
   gboolean visible;		/* either if layer visible or not */
} layer_t;


/*************
 * VARIABLES *
 *************/
extern draw_t drw;


/************
 * FUNTIONS *
 ************/

/* draw.c */
extern void create_draw(); 

void layer_add (gchar *name, gint r, gint g, gint b,
		gint line_type, gint line_anchor, gboolean visible);
void layer_change (guint n, gchar *name, gint r, gint g, gint b,
		   gint line_type, gint line_anchor, gboolean visible);
void layer_delete (guint n);
void create_basic_layers();


/* ui/ui.c */
extern void create_ui();
extern void update_ui();
