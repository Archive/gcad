/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include "../gtkglarea/gtkglarea.h"

#include "ui_global.h"
#include "../global.h"


/* winglXY_2_usrXY: convert GL window coords. to user coords., with snap */
void winglXY_2_usrXY (gint x_gl, gint y_gl, gdouble *x, gdouble *y)
{
   gint x_win, y_win, cx, cy;

   gdk_window_get_size (ui.gl->window, &x_win, &y_win);
   *x = (gdouble) x_gl * (drw.win_x2-drw.win_x1) / x_win + drw.win_x1;
   *y = (gdouble) (drw.win_y2-drw.win_y1) - y_gl * (drw.win_y2-drw.win_y1) / y_win + drw.win_y1;

   if (drw.snap)
   {
      cx = (gint) *x / drw.snap_x;
      cy = (gint) *y / drw.snap_y;
      *x = cx * drw.snap_x;
      *y = cy * drw.snap_y;
   }
}


/* usrXY_2_winglXY: convert user coords. to GL window coords. */
void usrXY_2_winglXY (gdouble x, gdouble y, gint *x_gl, gint *y_gl)
{
   gint x_win, y_win;	

   gdk_window_get_size (ui.gl->window, &x_win, &y_win);
   *x_gl = (gint) (x-drw.win_x1) * x_win / (drw.win_x2-drw.win_x1);
   *y_gl = (gint) (drw.win_y2-y) * y_win / (drw.win_y2-drw.win_y1);
}


/* fix_GLwin_ratio: fix GL window width / height ratio */
void fix_GLwin_ratio (gdouble w, gdouble h)
{
   gdouble m, d;

   /* recalculate GL window corners */
   if (w / h < (drw.win_x2-drw.win_x1) / (drw.win_y2-drw.win_y1))
   {
      /* x coords. well, change y */
      m = (drw.win_y1 + drw.win_y2) / 2;
      d = (drw.win_x2 - drw.win_x1) * h / (2 * w);
      drw.win_y1 = m - d;
      drw.win_y2 = m + d;
   }
   else
   {
      /* y coords. well, change x */
      m = (drw.win_x1 + drw.win_x2) / 2;
      d = (drw.win_y2 - drw.win_y1) * w / (2 * h);
      drw.win_x1 = m - d;
      drw.win_x2 = m + d;
   }

   /* update rulers range */
   gtk_ruler_set_range (GTK_RULER(ui.hrule), drw.win_x1, drw.win_x2,
                        0, drw.win_x2);
   gtk_ruler_set_range (GTK_RULER(ui.vrule), drw.win_y2, drw.win_y1,
                        0, drw.win_y2);

   /* update screen limits */
   drw.scr_x1 = (drw.win_x1 < drw.scr_x1) ? drw.win_x1 : drw.scr_x1;
   drw.scr_x2 = (drw.win_x2 > drw.scr_x2) ? drw.win_x2 : drw.scr_x2;
   drw.scr_y1 = (drw.win_y1 < drw.scr_y1) ? drw.win_y1 : drw.scr_y1;
   drw.scr_y2 = (drw.win_y2 > drw.scr_y2) ? drw.win_y2 : drw.scr_y2;
}
