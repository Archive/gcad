/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>

#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include "../gtkglarea/gtkglarea.h"

#include "ui_global.h"
#include "../global.h"


/*******************************
  Local Function prototypes
 *******************************/
void show_grid();


/*******************************
  init_gl
 *******************************/
void init_gl (GtkWidget *w)
{
   if (gtk_gl_area_begingl(GTK_GL_AREA(w))) 
   {
      glViewport (0, 0, w->allocation.width, w->allocation.height);
      glMatrixMode (GL_PROJECTION);
      glLoadIdentity();
      gluOrtho2D (drw.win_x1, drw.win_x2, drw.win_y1, drw.win_y2);
      glMatrixMode (GL_MODELVIEW);
      glLoadIdentity();
      gtk_gl_area_endgl (GTK_GL_AREA(w));
   }
}


/*******************************
  reshape_gl
 *******************************/
void reshape_gl (GtkWidget *w)
{
   if (gtk_gl_area_begingl(GTK_GL_AREA(w)))
   {
      glViewport(0,0, w->allocation.width, w->allocation.height);
      glMatrixMode (GL_PROJECTION);
      glLoadIdentity();
      gluOrtho2D (drw.win_x1, drw.win_x2, drw.win_y1, drw.win_y2);
      glMatrixMode (GL_MODELVIEW);
      glLoadIdentity();
      gtk_gl_area_endgl(GTK_GL_AREA(w));
   }
}


/*******************************
  draw_gl
 *******************************/
void draw_gl (GtkWidget *w)   
{
   /* simple drawing for test purposes */
   if (gtk_gl_area_begingl(GTK_GL_AREA(w)))
   {
      /* Clear GL window */
      glClearColor (0,0,0,1);
      glClear (GL_COLOR_BUFFER_BIT);

      /* Show grid */
      if (drw.grid)
         show_grid();

      /* Draw simple quadrant */
      glBegin (GL_QUADS);
      glColor3f (.1, .7, .1);
      glVertex2f (0, 0);
      glVertex2f (0, 100);
      glVertex2f (100, 100);
      glVertex2f (100, 0);
      glEnd();

      /* Draw simple triangle */
      glBegin (GL_TRIANGLES);
      glColor3f (.7, .3, .5);
      glVertex2f (200, 100);
      glVertex2f (200, 200);
      glVertex2f (300, 200);
      glEnd();

      gtk_gl_area_endgl(GTK_GL_AREA(w));
   }

   /* Swap backbuffer to front */
   gtk_gl_area_swapbuffers(GTK_GL_AREA(w));
}


/*******************************
  show_grid
 *******************************/
void show_grid()
{
   gdouble x, y, x0, y0;
	 
   glColor3f (.75, .45, .65);
   glPointSize (1);
   x0 = drw.win_x1 < 0 ? 0 : ceil(drw.win_x1 / drw.grid_x) * drw.grid_x;
   y0 = drw.win_y1 < 0 ? 0 : ceil(drw.win_y1 / drw.grid_y) * drw.grid_y;

   glBegin (GL_POINTS);
   for (x = x0; x <= drw.win_x2; x += drw.grid_x)
   {
      if (x < 0 || x > drw.width)
         continue;
      for (y = y0; y <= drw.win_y2; y += drw.grid_y)
      { 
         if (y < 0 || y > drw.height)
            continue;
         glVertex2f (x, y);
      }
   }
   glEnd();
}
