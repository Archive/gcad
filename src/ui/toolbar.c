/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>

#include "../global.h"

#include "ui_global.h"
#include "ui_cbs.h"


/*************
 * New types *
 *************/

typedef struct {
		 gchar         *text;
		 gchar         *tooltip;
		 gchar         *file;
		 GtkSignalFunc cb;
} toolbar_data_t;


/*************
 * Variables *
 *************/

static GnomeUIInfo main_tbar[] = {
  {GNOME_APP_UI_ITEM, N_("New"), N_("New drawing"),
   NULL, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_NEW, 0, 0, NULL},
  {GNOME_APP_UI_ITEM, N_("Open"), N_("Open a file"),
   NULL, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_OPEN, 0, 0, NULL},
  {GNOME_APP_UI_ITEM, N_("Save"), N_("Save file"),
   NULL, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE, 0, 0, NULL},
  {GNOME_APP_UI_ITEM, N_("Print"), N_("Print drawing"),
   NULL, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_PRINT, 0, 0, NULL},
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_SEPARATOR,
  /* layers combo */
  {GNOME_APP_UI_ITEM, N_("Layers"), N_("Edit layers"),
   NULL, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gcad/layers_edit.xpm",
   0, 0, NULL},
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_SEPARATOR,
  {GNOME_APP_UI_ITEM, N_("Undo"), N_("Undo last action"),
   NULL, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_UNDO,
   'z', GDK_CONTROL_MASK, NULL},
  {GNOME_APP_UI_ITEM, N_("Redo"), N_("Redo last action"),
   NULL, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_REDO, 0, 0, NULL},
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_SEPARATOR,
  {GNOME_APP_UI_ITEM, N_("Redraw"), N_("Redraw"),
   redraw_cb, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gcad/redraw.xpm",
   0, 0, NULL},
  /* zoom label */
  {GNOME_APP_UI_ITEM, N_("Zoom Window"), N_("Zoom Window"),
   zoom_win_cb, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_win.xpm",
   0, 0, NULL},
  {GNOME_APP_UI_ITEM, N_("Zoom Factor"), N_("Zoom Factor"),
   zoom_x_cb, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_x.xpm",
   0, 0, NULL},
  {GNOME_APP_UI_ITEM, N_("Zoom In"), N_("Zoom In"),
   zoom_in_cb, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_in.xpm",
   '+', GDK_MOD1_MASK, NULL},
  {GNOME_APP_UI_ITEM, N_("Zoom Out"), N_("Zoom Out"),
   zoom_out_cb, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_out.xpm",
   '-', GDK_MOD1_MASK, NULL},
  {GNOME_APP_UI_ITEM, N_("Zoom Previous"), N_("Previous Zoom"),
   zoom_prev, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_prev.xpm",
   0, 0, NULL},
  {GNOME_APP_UI_ITEM, N_("Zoom All"), N_("Zoom All"),
   zoom_all, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_all.xpm",
   0, 0, NULL},
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_END
};


static toolbar_data_t tbar1_els[] = {
   { N_("endpoint"), N_("End point"), "gcad/endpoint.xpm", NULL },
   { N_("midpoint"), N_("Mid point"), "gcad/midpoint.xpm", NULL },
   { N_("nearest"), N_("Nearest point"), "gcad/nearest.xpm", NULL },
   { N_("intersection"), N_("Intersection point"), "gcad/intersection.xpm", NULL },
   { N_("center"), N_("Center point"), "gcad/center.xpm", NULL },
   { N_("quadrant"), N_("Quadrant point"), "gcad/quadrant.xpm", NULL },
   { N_("tangent"), N_("Tangent point"), "gcad/tangent.xpm", NULL },
   { N_("perpendicular"), N_("Perpendicular point"), "gcad/perpendicular.xpm", NULL },
   { NULL, NULL, NULL }
};


static toolbar_data_t tbar2_els[] = {
   { N_("rubber"), N_("Rubber"), "gcad/rubber.xpm", NULL },
   { N_("point"), N_("Point"), "gcad/point.xpm", NULL },
   { N_("line"), N_("Line"), "gcad/line.xpm", NULL },
   { N_("circle"), N_("Circle"), "gcad/circle.xpm", NULL },
   { N_("ellipse"), N_("Ellipse"), "gcad/ellipse.xpm", NULL },
   { NULL, NULL, NULL }
};


static toolbar_data_t tbar3_els[] = {
   { "name", "tooltip", "tcd/rw.xpm", NULL },
   { NULL, NULL, NULL }
};


/*************
 * Functions *
 *************/
 
/* create_menu */
void create_main_toolbar()
{
   GtkStyle  *style;
   gchar     buf[20];
   
   gnome_app_create_toolbar (GNOME_APP(app), main_tbar);

   ui.cmb_layers = gtk_combo_new();
   gtk_widget_set_usize (GTK_COMBO(ui.cmb_layers)->entry, 100, 20);
   gtk_signal_connect_object (GTK_OBJECT(GTK_COMBO(ui.cmb_layers)->list),
			      "select_child",
			      (GtkSignalFunc) ui_layer_change,
			      NULL);
   gtk_widget_show (ui.cmb_layers);
   gtk_toolbar_insert_element (GTK_TOOLBAR(GNOME_APP(app)->toolbar),
                               GTK_TOOLBAR_CHILD_WIDGET, ui.cmb_layers, 
			       _("layers"), _("Layers"), NULL,
			       NULL,
			       NULL, NULL,
			       6);

   g_snprintf (buf, sizeof(buf), " ZOOM: %5.1f%% ", 100.0);
   ui.zoom = gtk_label_new (buf);
   style = gtk_style_new();
   style->font = gdk_font_load ("-misc-fixed-bold-r-normal--*-120-*-*-*-*-*-*");
   gdk_font_ref (style->font);
   gtk_widget_set_style (ui.zoom, style);
   gtk_widget_show (ui.zoom);
   gtk_toolbar_insert_element (GTK_TOOLBAR(GNOME_APP(app)->toolbar),
                               GTK_TOOLBAR_CHILD_WIDGET, ui.zoom, 
			       _("zoom"), _("Zoom"), NULL,
			       NULL,
			       NULL, NULL,
			       15);

   gtk_toolbar_set_style (GTK_TOOLBAR(GNOME_APP(app)->toolbar),
                          GTK_TOOLBAR_ICONS);
}

		 
void create_toolbar (GtkToolbar *tbar, toolbar_data_t *tbar_data)
{
   gchar     buf[255], *filename;
   GtkWidget *xpm;

   gtk_toolbar_append_space (tbar);   
   gtk_toolbar_append_space (tbar);   
   gtk_toolbar_append_space (tbar);   
   gtk_toolbar_append_space (tbar);   
   gtk_toolbar_append_space (tbar);   
   while (tbar_data->file)
   {
      g_snprintf (buf, sizeof(buf), tbar_data->file);
      filename = gnome_unconditional_pixmap_file (buf);
      xpm = gnome_pixmap_new_from_file (filename);
      g_free (filename);
      gtk_toolbar_append_item (tbar, 
                               tbar_data->text, tbar_data->tooltip, NULL,
                               xpm, tbar_data->cb, NULL);
      tbar_data++;
   }
}


void create_toolbar_1 (GtkToolbar *tbar)
{
   create_toolbar (tbar, tbar1_els);
}


void create_toolbar_2 (GtkToolbar *tbar)
{
   create_toolbar (tbar, tbar2_els);
}


void create_toolbar_3 (GtkToolbar *tbar)
{
   create_toolbar (tbar, tbar3_els);
}
