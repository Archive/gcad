/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include "../gtkglarea/gtkglarea.h"

#include <math.h>

#include "../global.h"
#include "ui_global.h"


#define ZOOM_X 0.25
#define ZOOM_Y 0.25


/*******************************
  Locals
 *******************************/
GdkPixmap *xpm_bak_x = NULL, *xpm_bak_y = NULL;		/* for pointer_line */
gint      x_gl_bak = -1, y_gl_bak = -1;
GdkPixmap *xpm_bak_wx = NULL, *xpm_bak_wy = NULL;	/* for window */
gdouble   wx1, wy1;
gboolean  wstate = FALSE;


/**************************************
  cmd_list_insert_text
 **************************************/
void cmd_list_insert_text (gchar *str, gboolean color)
{
   GdkColor *c;
   
   c = g_new (GdkColor, 1);
   c->red = 255 * (65535/255);
   c->green = 193 * (65535/255);
   c->blue = 37 * (65535/255);
   c->pixel = (gulong) (255*65536 + 193*256 + 37);
   gdk_color_alloc (gtk_widget_get_colormap(ui.cmd_list), c);

   gtk_text_freeze (GTK_TEXT(ui.cmd_list));
   gtk_widget_realize (ui.cmd_list);
   gtk_text_insert (GTK_TEXT(ui.cmd_list), NULL,
                    color ? c : &ui.cmd_list->style->white,
		    NULL,
                    str, -1);
   gtk_text_thaw (GTK_TEXT(ui.cmd_list));
   g_free (c);

   gtk_adjustment_set_value (ui.cmd_list_vadj, ui.cmd_list_vadj->upper - 52);
}


/**************************************
  show_pointer_lines
 **************************************/
void show_pointer_lines (gint x, gint y)
{
   gint w, h;

   w = ui.gl->allocation.width;
   h = ui.gl->allocation.height;

   /* restore old baks */
   if (xpm_bak_x)
   {
      gdk_draw_pixmap (ui.gl->window,
		       ui.gl->style->fg_gc[0],
		       xpm_bak_x,
		       0, 0,			/* source */
		       0, y_gl_bak,		/* dest */
		       w, 1);			/* size */
      gdk_draw_pixmap (ui.gl->window,
		       ui.gl->style->fg_gc[0],
		       xpm_bak_y,
		       0, 0,			/* source */
		       x_gl_bak, 0,		/* dest */
		       1, h);			/* size */
      g_free (xpm_bak_x);
      g_free (xpm_bak_y);
   }

   /* save new baks */
   x_gl_bak = x;
   y_gl_bak = y;
   xpm_bak_x = gdk_pixmap_new (ui.gl->window,
			       w, 1,
			       gdk_visual_get_system()->depth);
   gdk_draw_pixmap (xpm_bak_x,
		    ui.gl->style->fg_gc[0],
		    ui.gl->window,
		    0, y,			/* source */
		    0, 0,			/* dest */
		    w, 1);			/* size */
   xpm_bak_y = gdk_pixmap_new (ui.gl->window,
    			       1, h,
			       gdk_visual_get_system()->depth);
   gdk_draw_pixmap (xpm_bak_y,
		    ui.gl->style->fg_gc[0],
		    ui.gl->window,
		    x, 0,			/* source */
		    0, 0,			/* dest */
		    1, h);			/* size */

   /* show new cross lines */
   gdk_draw_line (ui.gl->window,
		  ui.gc1,
	          0, y, w, y);
   gdk_draw_line (ui.gl->window,
	          ui.gc1,
	          x, 0, x, h);
}


/**************************************
  show_window
 **************************************/
void show_window (gint x, gint y)
{
   gint x1, y1, x2, y2;

   /* restore contents of previous position */
   restore_window_baks();

   /* show new window rectangle (wx1, wy1) to (x, y) */
   gdk_draw_rectangle (ui.gl->window,
		       ui.gc2,
		       0,
		       MIN(wx1, x),
		       MIN(wy1, y),
		       abs(x-wx1),
		       abs(y-wy1)); 
}

void create_window_baks (gint x, gint y)
{
   gchar   buf[255];
   gint    w, h;
   gdouble x0, y0;

   wx1 = x;
   wy1 = y;
   wstate = TRUE;

   w = ui.gl->allocation.width;
   h = ui.gl->allocation.height;

   xpm_bak_wx = gdk_pixmap_new (ui.gl->window,
			        w, 1,
			        gdk_visual_get_system()->depth);
   if (xpm_bak_x)
      gdk_draw_pixmap (xpm_bak_wx,
		       ui.gl->style->fg_gc[0],
		       xpm_bak_x,
		       0, 0,			/* source */
		       0, 0,			/* dest */
		       w, 1);			/* size */
   xpm_bak_wy = gdk_pixmap_new (ui.gl->window,
    			        1, h,
			        gdk_visual_get_system()->depth);
   if (xpm_bak_y)
      gdk_draw_pixmap (xpm_bak_wy,
		       ui.gl->style->fg_gc[0],
		       xpm_bak_y,
		       0, 0,			/* source */
		       0, 0,			/* dest */
		       1, h);			/* size */

   winglXY_2_usrXY (wx1, wy1, &x0, &y0);
   g_snprintf (buf, sizeof(buf), "(%.1f,%.1f)", x0, y0);
   gtk_label_set (GTK_LABEL(ui.sbar5), buf);
   gtk_widget_show (ui.frame);
}

void restore_window_baks()
{
   gint w, h;

   w = ui.gl->allocation.width;
   h = ui.gl->allocation.height;

   /* restore old baks */
   gdk_draw_pixmap (ui.gl->window,
		    ui.gl->style->fg_gc[0],
		    xpm_bak_wx,
		    0, 0,			/* source */
		    0, wy1,			/* dest */
		    w, 1);			/* size */
   gdk_draw_pixmap (ui.gl->window,
		    ui.gl->style->fg_gc[0],
		    xpm_bak_wy,
		    0, 0,			/* source */
		    wx1, 0,			/* dest */
		    1, h);			/* size */
}

void free_window_baks()
{
   gtk_widget_hide (ui.frame);
   /* free all */
   wx1 = wy1 = 0;
   wstate = FALSE;
}


/**************************************
  show_circle
 **************************************/
void show_circle (gint x, gint y)
{
   /* show new circle  (wx1, wy1) to (x, y) */
#ifdef 0   
   gdk_draw_arc (ui.gl->window,
		 ui.gc2,
		 0,
		 wx1, wy1,
		 abs(x-wx1), abs(y-wy1),
		 0, 360); 
#endif
}


/**************************************
  Update scroll bars functions
 **************************************/
void change_gl_hscr()
{
   gdouble f, x0;
   
   /* Update horizontal scrollbar range */
   f = (drw.win_x2 - drw.win_x1) / (drw.scr_x2 - drw.scr_x1);
   ui.gl_hadj->page_size = f;
   x0 = (drw.win_x1 - drw.scr_x1) / (drw.scr_x2 - drw.scr_x1);
   gtk_adjustment_set_value (ui.gl_hadj, x0);
}

void change_gl_vscr()
{
   gdouble f, y0;
   
   /* Update vertical scrollbar range */
   f = (drw.win_y2 - drw.win_y1) / (drw.scr_y2 - drw.scr_y1);
   ui.gl_vadj->page_size = f;
   y0 = (drw.scr_y2 - drw.win_y2) / (drw.scr_y2 - drw.scr_y1);
   gtk_adjustment_set_value (ui.gl_vadj, y0);
}


/**************************************
  Zoom functions
 **************************************/
void zoom()
{
   gdouble x, y;

   /* fix GL window width / height ratio */
   fix_GLwin_ratio ((gdouble) (ui.gl->allocation).width,
                    (gdouble) (ui.gl->allocation).height);

   /* notify GL reshape event */
   reshape_gl (ui.gl);

   /* update scrollbars range */
   change_gl_hscr();
   change_gl_vscr();

   /* update new user coords for rulers */   
   winglXY_2_usrXY (x_gl_bak, y_gl_bak, &x, &y);
   /* update rulers range */
   gtk_ruler_set_range (GTK_RULER(ui.hrule), drw.win_x1, drw.win_x2,
                        x, drw.win_x2);
   gtk_ruler_set_range (GTK_RULER(ui.vrule), drw.win_y2, drw.win_y1,
                        y, drw.win_y2);

   /* redraw GL window contents */
   draw_gl (ui.gl);

   /* redraw pointer lines */
   xpm_bak_x = xpm_bak_y = NULL;	/* to avoid bak pixmaps recovering */
   show_pointer_lines (x_gl_bak, y_gl_bak);
}

void recalculate_zoom()
{
   gdouble w, h, c0, cn;
   gchar   buf[20];

   w = (gdouble) (ui.gl->allocation).width;
   h = (gdouble) (ui.gl->allocation).height;

   if (w / h < (drw.win_x2-drw.win_x1) / (drw.win_y2-drw.win_y1))
   {
      /* y coords. well */
      c0 = drw.width * 1.1;		/* ( (1 + 0.05) - (0 - 0.05) ) */
      cn = drw.win_x2 - drw.win_x1;
   }
   else
   {
      /* x coords. well */
      c0 = drw.height * 1.1;		/* ( (1 + 0.05) - (0 - 0.05) ) */
      cn = drw.win_y2 - drw.win_y1;
   }
   
   drw.zoom = 100 * c0 / cn;

   g_snprintf (buf, sizeof(buf), " ZOOM: %5.1f%% ", drw.zoom);
   gtk_label_set (GTK_LABEL(ui.zoom), buf);
}

 
void zoom_all()
{
   /* save previous zoom */
   drw.p_win_x1 = drw.win_x1;
   drw.p_win_y1 = drw.win_y1;
   drw.p_win_x2 = drw.win_x2;
   drw.p_win_y2 = drw.win_y2;

   /* update GL window coords. */
   drw.scr_x1 = drw.win_x1 = drw.org_x1;
   drw.scr_y1 = drw.win_y1 = drw.org_y1;
   drw.scr_x2 = drw.win_x2 = drw.org_x2;
   drw.scr_y2 = drw.win_y2 = drw.org_y2;

   /* do zoom */
   zoom();   
   recalculate_zoom();
   
   cmd_list_insert_text (_("Zoom All.\n"), FALSE);
}

void zoom_prev()
{
   gdouble x1, y1, x2, y2;
   
   x1 = drw.p_win_x1;
   y1 = drw.p_win_y1;
   x2 = drw.p_win_x2;
   y2 = drw.p_win_y2;
   
   /* save previous zoom */
   drw.p_win_x1 = drw.win_x1;
   drw.p_win_y1 = drw.win_y1;
   drw.p_win_x2 = drw.win_x2;
   drw.p_win_y2 = drw.win_y2;

   /* update GL window coords. */
   drw.win_x1 = x1;
   drw.win_y1 = y1;
   drw.win_x2 = x2;
   drw.win_y2 = y2;

   /* do zoom */
   zoom();   
   recalculate_zoom();
   
   cmd_list_insert_text (_("Previous Zoom.\n"), FALSE);
}

void zoom_in (gdouble x0, gdouble y0)
{
   gchar   buf[255];
   gdouble x, y;

   /* save previous zoom */
   drw.p_win_x1 = drw.win_x1;
   drw.p_win_y1 = drw.win_y1;
   drw.p_win_x2 = drw.win_x2;
   drw.p_win_y2 = drw.win_y2;

   winglXY_2_usrXY ((gint) x0, (gint) y0, &x, &y);

   drw.win_x1 = x - (x - drw.win_x1) * (1 - ZOOM_X);
   drw.win_y1 = y - (y - drw.win_y1) * (1 - ZOOM_Y);
   drw.win_x2 = x + (drw.win_x2 - x) * (1 - ZOOM_X);
   drw.win_y2 = y + (drw.win_y2 - y) * (1 - ZOOM_Y);

   /* do zoom */
   zoom();   
   recalculate_zoom();

   g_snprintf (buf, sizeof(buf), _("Zoom In at (%.2f,%.2f).\n"), x, y);
   cmd_list_insert_text (buf, FALSE);
}

void zoom_out (gdouble x0, gdouble y0)
{
   gchar   buf[255];
   gdouble x, y;

   /* save previous zoom */
   drw.p_win_x1 = drw.win_x1;
   drw.p_win_y1 = drw.win_y1;
   drw.p_win_x2 = drw.win_x2;
   drw.p_win_y2 = drw.win_y2;

   winglXY_2_usrXY ((gint) x0, (gint) y0, &x, &y);

   drw.win_x1 = x - (x - drw.win_x1) * (1 + ZOOM_X);
   drw.win_y1 = y - (y - drw.win_y1) * (1 + ZOOM_Y);
   drw.win_x2 = x + (drw.win_x2 - x) * (1 + ZOOM_X);
   drw.win_y2 = y + (drw.win_y2 - y) * (1 + ZOOM_Y);

   /* do zoom */
   zoom();   
   recalculate_zoom();

   g_snprintf (buf, sizeof(buf), _("Zoom Out at (%.2f,%.2f).\n"), x, y);
   cmd_list_insert_text (buf, FALSE);
}

void zoom_win (gdouble wx2, gdouble wy2)
{
   gchar   buf[255];
   gdouble win_x1, win_y1;
   gdouble win_x2, win_y2;

   /* save previous zoom */
   drw.p_win_x1 = drw.win_x1;
   drw.p_win_y1 = drw.win_y1;
   drw.p_win_x2 = drw.win_x2;
   drw.p_win_y2 = drw.win_y2;

   winglXY_2_usrXY ((gint) wx1, (gint) wy1, &win_x1, &win_y1);
   winglXY_2_usrXY ((gint) wx2, (gint) wy2, &win_x2, &win_y2);

   drw.win_x1 = MIN(win_x1, win_x2);
   drw.win_x2 = MAX(win_x1, win_x2);
   drw.win_y1 = MIN(win_y1, win_y2);
   drw.win_y2 = MAX(win_y1, win_y2);

   if ((drw.win_x2-drw.win_x1)/drw.width > (drw.win_y2-drw.win_y1)/drw.height)
   {
      gdouble size = (drw.win_x2-drw.win_x1) * (drw.height/drw.width);
      drw.win_y1 = (win_y1 + win_y2 - size) / 2;
      drw.win_y2 = drw.win_y1 + size;
      if (drw.win_y1 < drw.scr_y1)
      {
         drw.win_y1 = drw.scr_y1;
	 drw.win_y2 = drw.win_y1 + size;
      }
      if (drw.win_y2 > drw.scr_y2)
      {
	 drw.win_y2 = drw.scr_y2;
	 drw.win_y1 = drw.win_y2 - size;
      }
   }
   else
   {
      gdouble size = (drw.win_y2-drw.win_y1) * (drw.width/drw.height);
      drw.win_x1 = (win_x1 + win_x2 - size) / 2;
      drw.win_x2 = drw.win_x1 + size;
      if (drw.win_x1 < drw.scr_x1)
      {
         drw.win_x1 = drw.scr_x1;
	 drw.win_x2 = drw.win_x1 + size;
      }
      if (drw.win_x2 > drw.scr_x2)
      {
	 drw.win_x2 = drw.scr_x2;
	 drw.win_x1 = drw.win_x2 - size;
      }
   }

   /* do zoom */
   zoom();   
   recalculate_zoom();

   g_snprintf (buf, sizeof(buf), _("Zoom Window: (%.2f,%.2f) - (%.2f,%.2f).\n"),
               drw.win_x1, drw.win_y1, drw.win_x2, drw.win_y2);
   cmd_list_insert_text (buf, FALSE);
}


/**************************************
  Drawing Aids
 **************************************/
void toggle_polar()
{
   gchar   buf[50];

   drw.polar = drw.polar ? FALSE : TRUE;
   gdk_window_clear (ui.sbar6->window);
   gdk_draw_text (ui.sbar6->window, ui.sbar1->style->font,
	          ui.sbar6->style->black_gc,
	          drw.polar ? 6 : 10, 13, drw.polar ? "POLAR" : "CART ", 5);
   g_snprintf (buf, sizeof(buf), _("%s Coords.\n"),
               drw.polar ? _("Polar") : _("Cartesian"));
   cmd_list_insert_text (buf, FALSE);
}

void toggle_ortho()
{
   gchar   buf[50];

   drw.ortho = drw.ortho ? FALSE : TRUE;
   gdk_draw_text (ui.sbar2->window, ui.sbar1->style->font,
	          drw.ortho ? ui.sbar2->style->black_gc : ui.sbar2->style->white_gc,
	          6, 13, "ORTHO", 5);
   g_snprintf (buf, sizeof(buf), _("ORTHO %s.\n"), drw.ortho ? _("On") : _("Off"));
   cmd_list_insert_text (buf, FALSE);
}

void toggle_snap()
{
   gchar   buf[50];

   drw.snap = drw.snap ? FALSE : TRUE;
   gdk_draw_text (ui.sbar3->window, ui.sbar1->style->font,
	          drw.snap ? ui.sbar3->style->black_gc : ui.sbar3->style->white_gc,
	          7, 13, "SNAP", 4);
   g_snprintf (buf, sizeof(buf), _("SNAP %s.\n"), drw.snap ? _("On") : _("Off"));
   cmd_list_insert_text (buf, FALSE);
}

void toggle_grid()
{
   gchar   buf[50];

   drw.grid = drw.grid ? FALSE : TRUE;
   gdk_draw_text (ui.sbar4->window, ui.sbar1->style->font,
	          drw.grid ? ui.sbar4->style->black_gc : ui.sbar4->style->white_gc,
	          7, 13, "GRID", 4);
   g_snprintf (buf, sizeof(buf), _("GRID %s.\n"), drw.grid ? _("On") : _("Off"));
   cmd_list_insert_text (buf, FALSE);
}
