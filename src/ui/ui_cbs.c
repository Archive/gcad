/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>
#include <gdk/gdkkeysyms.h>

#include <math.h>

#include "../global.h"
#include "ui_global.h"
#include "ui_cbs.h"


/**************************************
  Locals 
 **************************************/
gdouble px, py;		/* pointer position */


/**************************************
  Functions 
 **************************************/
void toggle_grid_cb();


/**************************************
  Callbacks 
 **************************************/
void quit_reply_cb (gint reply)
{
   if (reply == GNOME_YES)
      gtk_main_quit();
}

gint delete_event_cb (GtkWidget * w, gpointer data)
{
   gchar buf[255];
   
   g_snprintf (buf, sizeof(buf), _("Quit '%s'?"), APPNAME);
   gnome_question_dialog_modal(buf, (GnomeReplyCallback) quit_reply_cb, NULL);
   return TRUE;
}


void about_cb()
{
   GtkWidget *ga;
   const gchar *authors[] = { "I�igo Serna <inigo@bipv02.bi.ehu.es>",
                        NULL };

   ga = gnome_about_new (PACKAGE,
                         VERSION,
			 COPYRIGHT_NOTICE,
                         authors,
			 _("Simple 2D CAD application for GNOME."),
			 NULL);
   gtk_widget_show (ga);
}


/* key pressed */
gint key_cb (GtkWidget *w, GdkEventKey *ev)
{
   gchar           buf[40], cmd[255];
   gdouble	   x, y;

   switch (ev->keyval)
   {
      case GDK_Return:
      case GDK_KP_Enter:
                g_snprintf (cmd, sizeof(cmd), "%s",
			    gtk_entry_get_text(GTK_ENTRY(ui.cmd)));
		if (g_strcasecmp(cmd, "") != 0)
		   cmd_list_insert_text (g_strconcat(cmd, "\n"), FALSE);
		gtk_entry_set_text (GTK_ENTRY(ui.cmd), "");
		break;
      case GDK_Escape:
		if (wstate)
                   free_window_baks();
                ui.mode = UI_NORMAL;
                change_cursor (CURSOR_NORMAL);
                show_pointer_lines (px, py);
		break;
      case GDK_F4:
                redraw_cb();
                show_pointer_lines (px, py);
		if (wstate)
	           show_window (px, py);
		break;
      case GDK_F6:
                toggle_polar();
		if (wstate)
		{
		    gdouble x0, y0, x, y;  
		    gchar   buf[50];

		    winglXY_2_usrXY (wx1, wy1, &x0, &y0);
		    winglXY_2_usrXY (px, py, &x, &y);
		    if (drw.polar)
		       g_snprintf (buf, sizeof(buf), "(%.1f,%.1f) <-> %.1f, %.1f�",
                                   x0, y0, hypot(x - x0, y - y0),
				   180.0 * atan2(y - y0, x - x0) / M_PI);
		    else
		       g_snprintf (buf, sizeof(buf), "(%.1f,%.1f) <-> %.1f,%.1f",
                                   x0, y0, x - x0, y - y0);
	            gtk_label_set (GTK_LABEL(ui.sbar5), buf);
		}    
		break;
      case GDK_F7:
                toggle_grid_cb();
		break;
      case GDK_F8:
                toggle_ortho();
		break;
      case GDK_F9:
                toggle_snap();
                winglXY_2_usrXY (px, py, &x, &y);
                g_snprintf (buf, sizeof(buf), " X: %6.2f  Y: %6.2f ", x, y);
                gtk_label_set (GTK_LABEL(ui.sbar1), buf);
		break;
      default:
                if (isprint(ev->keyval))
		{
                   g_snprintf (cmd, sizeof(cmd), "%c", ev->keyval);
                   gtk_entry_append_text (GTK_ENTRY(ui.cmd), cmd);
		}
		break;  	
   }      

}


/* mouse motion */
gint mouse_motion_cb (GtkWidget *w, GdkEventMotion *ev)
{
   gchar           buf[40];
   gint 	   x_gl, y_gl;
   GdkModifierType state;
   gdouble	   x, y, x0, y0;
   
   if (ev->is_hint)
   {
      gdk_window_get_pointer (ev->window, &x_gl, &y_gl, &state);
      px = (gdouble) x_gl;
      py = (gdouble) y_gl;
   }
   else
   {
      px = ev->x;
      py = ev->y;
      state = ev->state;
   }

   /* Update status bar XY */
   winglXY_2_usrXY (px, py, &x, &y);
   g_snprintf (buf, sizeof(buf), " X: %6.2f  Y: %6.2f ", x, y);
   gtk_label_set (GTK_LABEL(ui.sbar1), buf);

   /* Show pointer lines in XY */
   show_pointer_lines (px, py);

   /* If window activate => show rectangle and change mouse pointer */
   if (wstate)
   {
      switch (ui.mode)
      {
         case UI_NORMAL:
         case UI_ZOOM_WIN:
	         if (px < wx1)
		 {
		    if (py < wy1)
		       change_cursor (CURSOR_ZOOM_WIN_NW);
		    else
		       change_cursor (CURSOR_ZOOM_WIN_SW);
		 }
		 else
		 {
		    if (py < wy1)
		       change_cursor (CURSOR_ZOOM_WIN_NE);
		    else
		       change_cursor (CURSOR_ZOOM_WIN_SE);
                 }

	         show_window (px, py);

	         /* rectangle info */
		 winglXY_2_usrXY (wx1, wy1, &x0, &y0);
		 if (drw.polar)
		    g_snprintf (buf, sizeof(buf), "(%.1f,%.1f) <-> %.1f, %.1f�",
                                x0, y0, hypot(x - x0, y - y0),
				180.0 * atan2(y - y0, x - x0) / M_PI);
		 else
		    g_snprintf (buf, sizeof(buf), "(%.1f,%.1f) <-> %.1f,%.1f",
                                x0, y0, x - x0, y - y0);
	         gtk_label_set (GTK_LABEL(ui.sbar5), buf);
		 
		 break;

         case UI_ZOOM_X:
	         show_circle (px, py);

	         /* circle info */
		 winglXY_2_usrXY (wx1, wy1, &x0, &y0);
		 if (drw.polar)
		    g_snprintf (buf, sizeof(buf), "(%.1f,%.1f) <-> %.1f, %.1f�",
                                x0, y0, hypot(x - x0, y - y0),
				180.0 * atan2(y - y0, x - x0) / M_PI);
		 else
		    g_snprintf (buf, sizeof(buf), "(%.1f,%.1f) <-> %.1f,%.1f",
                                x0, y0, x - x0, y - y0);
	         gtk_label_set (GTK_LABEL(ui.sbar5), buf);

		 break;
       }
   }

   return TRUE;
}


/* button pressed */
gint button_press_cb (GtkWidget *w, GdkEventButton *ev)
{
   /* ignore button #2 */
   if (ev->button == 2)
      return TRUE;

   /* restore normal mode if button #3 */
   if (ev->button == 3)
   {
      if (wstate)
         free_window_baks();
      ui.mode = UI_NORMAL;
      change_cursor (CURSOR_NORMAL);
      show_pointer_lines (px, py);
      return TRUE;
   }

   switch (ui.mode)
   {
      case UI_NORMAL:
                create_window_baks (ev->x, ev->y);
		break;
      case UI_ZOOM_IN:
		zoom_in (ev->x, ev->y);
                ui.mode = UI_NORMAL;
                change_cursor (CURSOR_NORMAL);
		break;
      case UI_ZOOM_OUT:
		zoom_out (ev->x, ev->y);
                ui.mode = UI_NORMAL;
                change_cursor (CURSOR_NORMAL);
		break;
      case UI_ZOOM_X:
                create_window_baks (ev->x, ev->y);
		break;
      case UI_ZOOM_WIN:
                create_window_baks (ev->x, ev->y);
		break;
   }


   return TRUE;
}


/* button released */
gint button_release_cb (GtkWidget *w, GdkEventButton *ev)
{
   /* restore normal mode if button #2 or #3 */
   if (ev->button == 2 || ev->button == 3)
   {
      if (wstate)
         free_window_baks();
      ui.mode = UI_NORMAL;
      change_cursor (CURSOR_NORMAL);
      show_pointer_lines (px, py);
      return TRUE;
   }

   switch (ui.mode)
   {
      case UI_NORMAL:
                if (wstate)
		{
                   /* select_objects_in_area (ev->x, ev->y) */
		   restore_window_baks();
                   free_window_baks();
                   ui.mode = UI_NORMAL;
                   change_cursor (CURSOR_NORMAL);
                   show_pointer_lines (ev->x, ev->y);
		}
		break;
      case UI_ZOOM_X:
                if ((ev->x != wx1) && (ev->y != wy1))
/*		   zoom_x (ev->x, ev->y); */
		   zoom_win (ev->x, ev->y);
                free_window_baks();
                ui.mode = UI_NORMAL;
                change_cursor (CURSOR_NORMAL);
		break;
      case UI_ZOOM_WIN:
                if ((ev->x != wx1) && (ev->y != wy1))
		   zoom_win (ev->x, ev->y);
                free_window_baks();
                ui.mode = UI_NORMAL;
                change_cursor (CURSOR_NORMAL);
		break;
   }

   return TRUE;
}


/**************************************
  GL related callbacks
 **************************************/
gint realize_cb (GtkWidget *w)
{
   init_gl (w);
   return TRUE;
}

gint configure_cb (GtkWidget *wid, GdkEventConfigure *ev)
{
   /* restore baks */
   xpm_bak_x = xpm_bak_y = NULL;

   /* fix GL window width / height ratio */
   fix_GLwin_ratio ((gdouble) ev->width, (gdouble) ev->height);

   /* notify GL reshape event */
   reshape_gl (wid);

   return TRUE;
}

gint expose_cb (GtkWidget *w, GdkEventExpose *ev)
{
   /* draw only last expose. */
   if (ev->count > 0)
      return TRUE;

   /* draw contents */
   draw_gl (w);

   return TRUE;
}


/**************************************
  View callbacks
 **************************************/
void hscrbar_cb()
{
   gdouble size;

   /* calculate new position */
   size = drw.win_x2 - drw.win_x1;
   drw.win_x1 = drw.scr_x1 + ui.gl_hadj->value * (drw.scr_x2 - drw.scr_x1);
   drw.win_x2 = drw.win_x1 + size;
   /* redraw drawing */
   zoom();
}

void vscrbar_cb()
{
   gdouble size;

   /* calculate new position */
   size = drw.win_y2 - drw.win_y1;
   drw.win_y2 = drw.scr_y2 - ui.gl_vadj->value * (drw.scr_y2 - drw.scr_y1);
   drw.win_y1 = drw.win_y2 - size;
   /* redraw drawing */
   zoom();
}


void redraw_cb()
{
   draw_gl (ui.gl);
   cmd_list_insert_text (_("Redraw.\n"), FALSE);
}

void zoom_x_cb()
{
   if (ui.mode != UI_NORMAL)
      return;

   ui.mode = UI_ZOOM_X;
   change_cursor (CURSOR_ZOOM_X);
}

void zoom_win_cb()
{
   if (ui.mode != UI_NORMAL)
      return;

   ui.mode = UI_ZOOM_WIN;
   change_cursor (CURSOR_ZOOM_WIN_NW);
}

void zoom_in_cb()
{
   if (ui.mode != UI_NORMAL)
      return;
      
   ui.mode = UI_ZOOM_IN;
   change_cursor (CURSOR_ZOOM_IN);
}

void zoom_out_cb()
{
   if (ui.mode != UI_NORMAL)
      return;
      
   ui.mode = UI_ZOOM_OUT;
   change_cursor (CURSOR_ZOOM_OUT);
}


/**************************************
  Drawing Aids callbacks
 **************************************/
void toggle_grid_cb()
{ 
   toggle_grid();
   xpm_bak_x = xpm_bak_y = NULL;
   draw_gl(ui.gl);
   show_pointer_lines (px, py);
}   


/**************************************
  Layers UI functions
 **************************************/
/* ui_layer_change */
void ui_layer_change ()
{
   GList   *layers_list;
   layer_t *l;
   gchar   *layer_name;
   gint    i;
   
   layer_name = gtk_entry_get_text (GTK_ENTRY(GTK_COMBO(ui.cmb_layers)->entry));
   layers_list = drw.layers;
   for (i = 0; layers_list; i++, layers_list = layers_list->next)
   {
      l = layers_list->data;
      if (strcmp(l->name, layer_name) == 0)
         break;  
   }
   drw.layer_i = i < drw.layers_num ? i : 0;
}
