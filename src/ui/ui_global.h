/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>


/*************************
  Types
 **************************/

/* UI MODE */
typedef enum {
   UI_NORMAL,

   UI_ZOOM_WIN,
   UI_ZOOM_X,
   UI_ZOOM_IN,
   UI_ZOOM_OUT
} ui_mode_t;


/* UI KEYBOARD MODE */
typedef enum {
   KBD_ERROR,
   KBD_COMMAND,
   KBD_POINT,
   KBD_NUMBER
} kbd_mode_t;



/* UI */
typedef struct {
   GtkWidget     *tbar1;
   GtkWidget     *hrule;
   GtkWidget     *vrule;
   GtkWidget     *gl;
   GdkGC         *gc1, *gc2;
   GtkAdjustment *gl_hadj;
   GtkWidget     *hscrbar;
   GtkAdjustment *gl_vadj;
   GtkWidget     *vscrbar;
   GtkWidget     *tbar2;
   GtkWidget     *tbar3;

   GtkWidget     *cmd_list;
   GtkAdjustment *cmd_list_vadj;
   GtkWidget     *cmd_list_vscrbar;
   GtkWidget     *cmd;

   GtkWidget     *sbar1;
   GtkWidget     *sbar6;
   GtkWidget     *sbar2;
   GtkWidget     *sbar3;
   GtkWidget     *sbar4;
   GtkWidget     *frame;
   GtkWidget     *sbar5;

   GtkWidget     *cmb_layers;
   GtkWidget     *zoom;
   
   ui_mode_t     mode;
   kbd_mode_t    kbd;
} ui_t;


/* CURSORS */
enum {
   CURSOR_NORMAL = GDK_CROSSHAIR,
   CURSOR_DRAW = GDK_PENCIL,
   CURSOR_ZOOM_IN = GDK_TARGET,
   CURSOR_ZOOM_OUT = GDK_TARGET,
   CURSOR_ZOOM_X = GDK_TARGET,
   CURSOR_ZOOM_WIN_NW = GDK_UL_ANGLE,
   CURSOR_ZOOM_WIN_NE = GDK_UR_ANGLE,
   CURSOR_ZOOM_WIN_SW = GDK_LL_ANGLE,
   CURSOR_ZOOM_WIN_SE = GDK_LR_ANGLE
};

		    
/*************
 * Variables *
 *************/

extern GtkWidget *app;
extern ui_t	 ui;
extern GdkPixmap *xpm_bak_x, *xpm_bak_y;	/* for show_pointer_lines */
extern gdouble	 wx1, wy1;			/* for window */
extern gboolean  wstate;			/*     "      */


/*************
 * Functions *
 *************/

/* ui.c */
extern void change_cursor (gint cursor);

/* ui_actions.c */
extern void cmd_list_insert_text (gchar *str, gboolean color);

extern void show_pointer_lines (gint x, gint y);
extern void show_window (gint x, gint y);
extern void create_window_baks (gint x, gint y);
extern void restore_window_baks();
extern void free_window_baks();
extern void show_circle (gint x, gint y);

extern void change_gl_hscr();
extern void change_gl_vscr();

extern void recalculate_zoom();
extern void zoom();
extern void zoom_all();
extern void zoom_prev();
extern void zoom_in (gdouble x0, gdouble y0);
extern void zoom_out (gdouble x0, gdouble y0);
extern void zoom_win (gdouble wx2, gdouble wy2);

extern void toggle_polar();
extern void toggle_ortho();
extern void toggle_snap();
extern void toggle_grid();

/* menu.c */
extern void create_main_menu();

/* toolbar.c */
extern void create_main_toolbar();
extern void create_toolbar_1 (GtkToolbar *tbar);
extern void create_toolbar_2 (GtkToolbar *tbar);
extern void create_toolbar_3 (GtkToolbar *tbar);

/* util.c */
extern void winglXY_2_usrXY (gint x_gl, gint y_gl, gdouble *x, gdouble *y);
extern void usrXY_2_winglXY (gdouble x, gdouble y, gint *x_gl, gint *y_gl);
extern void fix_GLwin_ratio (gdouble w, gdouble h);

/* gl.c */
extern void init_gl (GtkWidget *w);
extern void reshape_gl (GtkWidget *w);
extern void draw_gl (GtkWidget *w);
