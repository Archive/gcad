/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/**************************************
  Callbacks Prototypes
 **************************************/

extern gint delete_event_cb (GtkWidget * w, gpointer data);

extern void about_cb();

extern gint key_cb (GtkWidget *w, GdkEventKey *ev);
extern gint mouse_motion_cb (GtkWidget *w, GdkEventMotion *ev);
extern gint button_press_cb (GtkWidget *w, GdkEventButton *ev);
extern gint button_release_cb (GtkWidget *w, GdkEventButton *ev);

/* GL related callbacks */
extern gint realize_cb (GtkWidget *w);
extern gint configure_cb (GtkWidget *w, GdkEventConfigure *ev);
extern gint expose_cb (GtkWidget *w, GdkEventExpose *ev);

/* View callbacks */
extern void hscrbar_cb();
extern void vscrbar_cb();

extern void redraw_cb();
extern void zoom_win_cb();
extern void zoom_x_cb();
extern void zoom_in_cb();
extern void zoom_out_cb();

/* Drawing Aids callbacks */
extern void toggle_grid_cb();

/* Layers UI callbacks */
extern void ui_layer_change();
