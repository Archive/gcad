/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>

#include "ui_global.h"
#include "ui_cbs.h"


/****************
 * Menu entries *
 ****************/

/* gCAD */
static GnomeUIInfo gcad_menu[] = {
   {GNOME_APP_UI_ITEM, N_("Properties..."), N_("Properties"), 
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PROP, 0, 0, NULL},
   GNOMEUIINFO_END
};

 
/* File */
static GnomeUIInfo file_menu[] = {
   {GNOME_APP_UI_ITEM, N_("New"), 
    N_("New drawing"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    'n',  GDK_MOD1_MASK, NULL },
   {GNOME_APP_UI_ITEM, N_("Open"), 
    N_("Open a file"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
    'o', GDK_CONTROL_MASK, NULL },
   {GNOME_APP_UI_ITEM, N_("Save"), 
    N_("Save file"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE,
    's', GDK_CONTROL_MASK, NULL },
   {GNOME_APP_UI_ITEM, N_("Save As..."), 
    N_("Save file with new name"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
    0, 0, NULL },
   {GNOME_APP_UI_SEPARATOR, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Import..."), 
    N_("Import file"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Export..."), 
    N_("Export file"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   {GNOME_APP_UI_SEPARATOR, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Print..."), 
    N_("Print drawing"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PRINT,
    'p', GDK_CONTROL_MASK, NULL },
   {GNOME_APP_UI_SEPARATOR, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Exit"), 
    N_("Quit the application"),
    delete_event_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT,
    'q', GDK_CONTROL_MASK, NULL },
   GNOMEUIINFO_END
};

/* Edit */
static GnomeUIInfo snap_submenu[] = {
   {GNOME_APP_UI_ITEM, N_(" End point"), 
    N_("End point"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/endpoint.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Mid point"), 
    N_("Mid point"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/midpoint.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Nearest point"), 
    N_("Nearest point"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/nearest.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Intersection"), 
    N_("Intersection point"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/intersection.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Center point"), 
    N_("Center point"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/center.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Quadrant"), 
    N_("Quadrant point"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/quadrant.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Tangent"), 
    N_("Tangent point"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/tangent.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Perpendicular point"), 
    N_("Perpendicular point"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/perpendicular.xpm",
    0, 0, NULL},
   GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[] = {
   {GNOME_APP_UI_ITEM, N_("Undo"), 
    N_("Undo last action"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UNDO, 
    'z', GDK_CONTROL_MASK, NULL },
   {GNOME_APP_UI_ITEM, N_("Redo"), 
    N_("Redo last action"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_REDO,
    0, 0, NULL },
   {GNOME_APP_UI_SEPARATOR, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Cut"), 
    N_("Undo last action"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CUT,
    'x', GDK_CONTROL_MASK, NULL },
   {GNOME_APP_UI_ITEM, N_("Copy"), 
    N_("Copy"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_COPY,
    'c', GDK_CONTROL_MASK, NULL },
   {GNOME_APP_UI_ITEM, N_("Paste"), 
    N_("Paste"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PASTE,
    'v', GDK_CONTROL_MASK, NULL },
   {GNOME_APP_UI_ITEM, N_("Delete"), 
    N_("Delete"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   {GNOME_APP_UI_SEPARATOR, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
   GNOMEUIINFO_SUBTREE (N_("Snap"), snap_submenu),
   {GNOME_APP_UI_SEPARATOR, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Layers..."), 
    N_("Edit layers"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/layers_edit.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Object properties..."), 
    N_("Object properties..."),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   GNOMEUIINFO_END
};

/* View */
static GnomeUIInfo view_menu[] = {
   {GNOME_APP_UI_ITEM, N_("Redraw"), 
    N_("Redraw drawing"),
    redraw_cb, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/redraw.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_SEPARATOR, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Zoom Window"), 
    N_("Zoom Window"),
    zoom_win_cb, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_win.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Zoom Factor"), 
    N_("Zoom Factor"),
    zoom_x_cb, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_x.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Zoom In"), 
    N_("Zoom In"),
    zoom_in_cb, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_in.xpm",
    '+', GDK_MOD1_MASK, NULL},
   {GNOME_APP_UI_ITEM, N_(" Zoom Out"), 
    N_("Zoom Out"),
    zoom_out_cb, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_out.xpm",
    '-', GDK_MOD1_MASK, NULL},
   {GNOME_APP_UI_ITEM, N_(" Zoom Previous"), 
    N_("Previous Zoom"),
    zoom_prev, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_prev.xpm",
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_(" Zoom All"), 
    N_("Zoom All"),
    zoom_all, NULL, NULL,
    GNOME_APP_PIXMAP_FILENAME, "gcad/zoom_all.xpm",
    0, 0, NULL},
   GNOMEUIINFO_END
};

/* Draw */
static GnomeUIInfo draw_menu[] = {
   GNOMEUIINFO_END
};

/* Modify */
static GnomeUIInfo modify_menu[] = {
   GNOMEUIINFO_END
};

/* Inquiry */
static GnomeUIInfo inquiry_menu[] = {
   {GNOME_APP_UI_ITEM, N_("Distance"), 
    N_("Distance"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Angle"), 
    N_("Angle"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Area"), 
    N_("Area"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("Center of Gravity"), 
    N_("Center of Gravity"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, 0, NULL},
   GNOMEUIINFO_END
};

/* Scripts */
static GnomeUIInfo scripts_menu[] = {
   GNOMEUIINFO_END
};

/* Help */
static GnomeUIInfo help_menu[] = {
   {GNOME_APP_UI_HELP, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
   {GNOME_APP_UI_SEPARATOR, NULL, NULL, NULL, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
   {GNOME_APP_UI_ITEM, N_("About..."), N_("Tell about this application"), 
    about_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT, 0, 0, NULL},
   GNOMEUIINFO_END
};

/* MAIN */
static GnomeUIInfo main_menu[] = {
   {GNOME_APP_UI_SUBTREE, "gCAD", NULL, gcad_menu, NULL, NULL,
/*    GNOME_APP_PIXMAP_FILENAME, "gnome-default.png", 0, 0, NULL}, */
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BOOK_RED, 0, 0, NULL},
   GNOMEUIINFO_SUBTREE (N_("File"), file_menu),
   GNOMEUIINFO_SUBTREE (N_("Edit"), edit_menu),
   GNOMEUIINFO_SUBTREE (N_("View"), view_menu),
   GNOMEUIINFO_SUBTREE (N_("Draw"), draw_menu),
   GNOMEUIINFO_SUBTREE (N_("Modify"), modify_menu),
   GNOMEUIINFO_SUBTREE (N_("Inquiry"), inquiry_menu),
   GNOMEUIINFO_SUBTREE (N_("Scripts"), scripts_menu),
   {GNOME_APP_UI_JUSTIFY_RIGHT},
   {GNOME_APP_UI_SUBTREE, N_("Help"), NULL, help_menu, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_BUTTON_HELP, 0, 0, NULL},
   GNOMEUIINFO_END
};


/*************
 * Functions *
 *************/
 
/* create_menu */
void create_main_menu()
{
   gnome_app_create_menus (GNOME_APP(app), main_menu);
}
