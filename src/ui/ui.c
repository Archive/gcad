/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>

#include <math.h>
#include <string.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include "../gtkglarea/gtkglarea.h"

#include "../global.h"
#include "ui_global.h"
#include "ui_cbs.h"


/*******************************
  Globals
 *******************************/
GtkWidget *app;
ui_t	  ui;

/*******************************
  Locals
 *******************************/
GtkWidget *app_box;


/*******************************
  Local Function prototypes
 *******************************/
gint app_expose_cb (GtkWidget *w, GdkEventExpose *ev);


/**************************************
  Set up the GUI
 **************************************/
void create_ui()
{
   GtkWidget       *hbox;
   GtkWidget       *table;
   GdkColorContext *cc;
   GdkColor  	   *c;
   GtkWidget 	   *hsep;
   GtkWidget 	   *frame;
   GtkStyle  	   *style;
   GtkWidget 	   *evbox;
   gint      	   width, height, n;
   gint gl_attrlist[] = {
      GDK_GL_RGBA,
      GDK_GL_DOUBLEBUFFER,
      GDK_GL_NONE
   };

   /* GL initialization */
   if (gdk_gl_query() == FALSE)
      g_error (_("OpenGL not supported\n"));
    
   /* Initialization */
   app = gnome_app_new (APPNAME, APPNAME);
   gtk_widget_set_name (GTK_WIDGET(app), APPNAME);
   gtk_window_set_wmclass (GTK_WINDOW(app), APPNAME, APPNAME);
   gtk_window_set_policy (GTK_WINDOW(app), TRUE, TRUE, TRUE);
/*   gtk_widget_set_usize (GTK_WIDGET(app), 800, 600); */
  
   gtk_signal_connect (GTK_OBJECT(app), "destroy",
                       GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
   gtk_signal_connect (GTK_OBJECT(app), "delete_event",
                       GTK_SIGNAL_FUNC(delete_event_cb), NULL);
   gtk_signal_connect (GTK_OBJECT(app), "expose_event",
		       GTK_SIGNAL_FUNC(app_expose_cb), NULL);
   gtk_signal_connect (GTK_OBJECT(app), "key_press_event",
		       GTK_SIGNAL_FUNC(key_cb), NULL);
   gtk_widget_set_events (app, GDK_EXPOSURE_MASK
                          | GDK_KEY_PRESS_MASK);
   gtk_widget_realize (app);
   gnome_win_hints_init();
 

   /* For gtkGLarea, specific */
   gtk_quit_add_destroy (1, GTK_OBJECT(app));
  
   
   app_box = gtk_vbox_new (FALSE, 0);
   gtk_container_border_width (GTK_CONTAINER(app_box), GNOME_PAD_SMALL);
   gnome_app_set_contents (GNOME_APP(app), app_box);


   /* Horizontal box for 3 toolbars and a scrollwindow for gl window */
   hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
   gtk_box_pack_start (GTK_BOX (app_box), hbox,
                       TRUE, TRUE, GNOME_PAD_SMALL);
   gtk_widget_show (hbox);
   ui.tbar1 = gtk_toolbar_new (GTK_ORIENTATION_VERTICAL, GTK_TOOLBAR_ICONS);
   gtk_box_pack_start (GTK_BOX(hbox), ui.tbar1, FALSE, FALSE, 0);
   gtk_widget_show (ui.tbar1);
   create_toolbar_1 ((GtkToolbar *) ui.tbar1);

   table = gtk_table_new (3, 3, FALSE);
   gtk_container_border_width (GTK_CONTAINER(table), GNOME_PAD_SMALL);
   gtk_box_pack_start (GTK_BOX(hbox), table, TRUE, TRUE, 0);
   gtk_widget_show (table);

   ui.tbar2 = gtk_toolbar_new (GTK_ORIENTATION_VERTICAL, GTK_TOOLBAR_ICONS);
   gtk_box_pack_start (GTK_BOX(hbox), ui.tbar2, FALSE, FALSE, 0);
   gtk_widget_show (ui.tbar2);
   create_toolbar_2 ((GtkToolbar *) ui.tbar2);

   ui.tbar3 = gtk_toolbar_new (GTK_ORIENTATION_VERTICAL, GTK_TOOLBAR_ICONS);
   gtk_box_pack_start (GTK_BOX(hbox), ui.tbar3, FALSE, FALSE, 0);
   gtk_widget_show (ui.tbar3);
   create_toolbar_3 ((GtkToolbar *) ui.tbar3);
   
   /* Drawing area: GL window, rulers and scrollbars */
   ui.gl = gtk_gl_area_new (gl_attrlist);
   width = 600;
   height = width * drw.height/drw.width * gdk_screen_height_mm()/gdk_screen_width_mm();
/*   height = width * gdk_screen_height_mm() / gdk_screen_width_mm(); */
   gtk_widget_set_usize (GTK_WIDGET(ui.gl), width, height);
   gtk_table_attach (GTK_TABLE (table), ui.gl, 1, 2, 1, 2,
                     GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
   gtk_signal_connect (GTK_OBJECT(ui.gl), "realize",
		       (GtkSignalFunc) realize_cb, NULL);
   gtk_signal_connect (GTK_OBJECT(ui.gl), "configure_event",
		       (GtkSignalFunc) configure_cb, NULL);
   gtk_signal_connect (GTK_OBJECT(ui.gl), "expose_event",
 		       (GtkSignalFunc) expose_cb, NULL);
   gtk_signal_connect (GTK_OBJECT(ui.gl), "motion_notify_event",
		       (GtkSignalFunc) mouse_motion_cb, NULL);
   gtk_signal_connect (GTK_OBJECT(ui.gl), "button_press_event",
		       (GtkSignalFunc) button_press_cb, NULL);
   gtk_signal_connect (GTK_OBJECT(ui.gl), "button_release_event",
		       (GtkSignalFunc) button_release_cb, NULL);
/*   gtk_signal_connect (GTK_OBJECT(ui.gl), "leave_notify_event",
		       (GtkSignalFunc) NULL, NULL);
   gtk_signal_connect (GTK_OBJECT(ui.gl), "proximity_out_event",
		       (GtkSignalFunc) NULL, NULL);
   gtk_widget_set_events (ui.gl, GDK_LEAVE_NOTIFY_MASK
			  | GDK_PROXIMITY_OUT_MASK); */
   gtk_widget_set_events (ui.gl, GDK_EXPOSURE_MASK
			  | GDK_BUTTON_PRESS_MASK
			  | GDK_BUTTON_RELEASE_MASK
                          | GDK_POINTER_MOTION_MASK
			  | GDK_POINTER_MOTION_HINT_MASK);
   gtk_widget_show (ui.gl);
   /* create new GCs for GL window pointer lines and window rectangle */
   gtk_widget_realize (ui.gl);   

   cc = gdk_color_context_new (gtk_widget_get_visual(app),
                               gtk_widget_get_colormap(app));
   ui.gc1 = gdk_gc_new (ui.gl->window);
   c = g_new (GdkColor, 1);
   c->red = 220 * (65535/255);
   c->green = 220 * (65535/255);
   c->blue = 220 * (65535/255);
   c->pixel = (gulong) 0;
   n = 0;
   gdk_color_context_get_pixels (cc, &c->red, &c->green, &c->blue, 1,
                                    &c->pixel, &n );
   gdk_gc_set_foreground (ui.gc1, c);
   g_free (c);
   gdk_gc_set_line_attributes (ui.gc1, 1, GDK_LINE_ON_OFF_DASH,
                               GDK_CAP_ROUND, GDK_JOIN_ROUND);
   ui.gc2 = gdk_gc_new (ui.gl->window);
   c = g_new (GdkColor, 1);
   c->red = 255 * (65535/255);
   c->green = 20 * (65535/255);
   c->blue = 20 * (65535/255);
   c->pixel = (gulong) 0;
   n = 0;
   gdk_color_context_get_pixels (cc, &c->red, &c->green, &c->blue, 1,
                                    &c->pixel, &n );
   gdk_gc_set_foreground (ui.gc2, c);
   g_free (c);
   gdk_gc_set_line_attributes (ui.gc2, 1, GDK_LINE_ON_OFF_DASH,
                               GDK_CAP_ROUND, GDK_JOIN_ROUND);

   /* rulers */
   ui.hrule = gtk_hruler_new();
   gtk_ruler_set_range (GTK_RULER(ui.hrule), 0, 0, 0, 0);
   gtk_signal_connect_object (GTK_OBJECT(ui.gl), "motion_notify_event",
			      (GtkSignalFunc) GTK_WIDGET_CLASS(GTK_OBJECT(ui.hrule)->klass)->motion_notify_event,
                              GTK_OBJECT(ui.hrule));
   gtk_table_attach (GTK_TABLE(table), ui.hrule, 1, 2, 0, 1,
                     GTK_FILL, GTK_FILL, 0, 0);
   gtk_widget_show (ui.hrule);
   ui.vrule = gtk_vruler_new();
   gtk_ruler_set_range (GTK_RULER(ui.vrule), 0, 0, 0, 0);
   gtk_signal_connect_object (GTK_OBJECT(ui.gl), "motion_notify_event",
			      (GtkSignalFunc) GTK_WIDGET_CLASS(GTK_OBJECT(ui.vrule)->klass)->motion_notify_event,
                              GTK_OBJECT(ui.vrule));
   gtk_table_attach (GTK_TABLE(table), ui.vrule, 0, 1, 1, 2,
                     GTK_FILL, GTK_FILL, 0, 0);
   gtk_widget_show (ui.vrule);
   /* change font for rulers */
   style = gtk_style_new();
   style->font = gdk_font_load ("-misc-fixed-medium-r-semicondensed--*-100-*-*-*-*-*-*");
   gdk_font_ref (style->font);
   gtk_widget_set_style (ui.hrule, style);
   gtk_widget_set_style (ui.vrule, style);

   /* scroll bars */
   ui.gl_hadj = (GtkAdjustment *) gtk_adjustment_new(0.0, 0.0, 1.0,
                                                     0.01, 0.1, 0.1);
   gtk_signal_connect_object (GTK_OBJECT(ui.gl_hadj), "value_changed",
			      (GtkSignalFunc) hscrbar_cb,
			      NULL);
   ui.hscrbar = gtk_hscrollbar_new (ui.gl_hadj);
   gtk_table_attach (GTK_TABLE(table), ui.hscrbar, 1, 2, 2, 3,
                     GTK_FILL, GTK_FILL, 0, 0);
   gtk_widget_show (ui.hscrbar);

   ui.gl_vadj = (GtkAdjustment *) gtk_adjustment_new(0.0, 0.0, 1.0,
                                                     0.01, 0.1, 0.1);
   gtk_signal_connect_object (GTK_OBJECT(ui.gl_vadj), "value_changed",
			      (GtkSignalFunc) vscrbar_cb,
			      NULL);
   ui.vscrbar = gtk_vscrollbar_new (ui.gl_vadj);
   gtk_table_attach (GTK_TABLE(table), ui.vscrbar, 2, 3, 1, 2,
                     GTK_FILL, GTK_FILL, 0, 0);
   gtk_widget_show (ui.vscrbar);

   /* horizontal separator */
   hsep = gtk_hseparator_new();
   gtk_box_pack_start (GTK_BOX (app_box), hsep, FALSE, FALSE, 0);
   gtk_widget_show (hsep);
   
   /* table for edit and entry widgets */
   table = gtk_table_new (2, 2, FALSE);
   gtk_box_pack_start (GTK_BOX (app_box), table,
                       FALSE, FALSE, GNOME_PAD_SMALL);
   gtk_widget_show (table);
   ui.cmd_list_vadj = (GtkAdjustment *) gtk_adjustment_new(0.0, 0.0, 0.0,
                                                           13.0, 45.0, 0.0);
   ui.cmd_list = gtk_text_new (FALSE, ui.cmd_list_vadj);
   gtk_widget_set_usize (GTK_WIDGET(ui.cmd_list), 400, 45);
   gtk_text_set_editable (GTK_TEXT(ui.cmd_list), FALSE);
   gtk_table_attach (GTK_TABLE(table), ui.cmd_list, 0, 1, 0, 1,
                     GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
   gtk_widget_show (ui.cmd_list);
   ui.cmd_list_vscrbar = gtk_vscrollbar_new (ui.cmd_list_vadj);
   gtk_table_attach (GTK_TABLE(table), ui.cmd_list_vscrbar, 1, 2, 0, 1,
                     GTK_FILL, GTK_FILL, 0, 0);
   gtk_widget_show (ui.cmd_list_vscrbar);
   ui.cmd = gtk_entry_new();
   gtk_table_attach (GTK_TABLE(table), ui.cmd, 0, 2, 1, 2,
                     GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
   gtk_widget_show (ui.cmd);
   GTK_WIDGET_SET_FLAGS (ui.cmd, GTK_CAN_DEFAULT);
   gtk_widget_grab_focus (ui.cmd);
   gtk_widget_grab_default (ui.cmd);
   /* color and font for text and entry widgets */
   style = gtk_style_new();
   style->font = gdk_font_load ("-misc-fixed-medium-r-semicondensed--*-100-*-*-*-*-*-*");
   gdk_font_ref (style->font);
   style->base[GTK_STATE_NORMAL] = ui.cmd_list->style->black;
   style->fg[GTK_STATE_NORMAL] = ui.cmd_list->style->white;
   gtk_widget_set_style (ui.cmd_list, style);
   gtk_widget_set_style (ui.cmd, style);

   /* horizontal separator */
   hsep = gtk_hseparator_new();
   gtk_box_pack_start (GTK_BOX (app_box), hsep, FALSE, FALSE, 0);
   gtk_widget_show (hsep);
   
   /* hbox for status bars */
   hbox = gtk_hbox_new (FALSE, 2);
   gtk_box_pack_start (GTK_BOX (app_box), hbox,
                       FALSE, FALSE, 2);
   gtk_widget_show (hbox);
   /* XY status bar */
   frame = gtk_frame_new (NULL);
   gtk_container_border_width (GTK_CONTAINER(frame), 0);
   gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_IN);
   gtk_widget_set_usize (frame, 190, 15);
   gtk_box_pack_start (GTK_BOX(hbox), frame, FALSE, FALSE, 0);
   gtk_widget_show (frame);
   ui.sbar1 = gtk_label_new ("");
   gtk_container_add (GTK_CONTAINER(frame), ui.sbar1);
   gtk_widget_show (ui.sbar1);
   style = gtk_style_new();
   style->font = gdk_font_load ("-misc-fixed-bold-r-normal--*-100-*-*-*-*-*-*");
   gdk_font_ref (style->font);
   gtk_widget_set_style (ui.sbar1, style);
   /* cart/polar coords. status bar */
   frame = gtk_frame_new (NULL);
   gtk_container_border_width (GTK_CONTAINER(frame), 0);
   gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_IN);
   gtk_box_pack_start (GTK_BOX(hbox), frame, FALSE, FALSE, 2);
   gtk_widget_show (frame);
   evbox = gtk_event_box_new();
   gtk_container_add (GTK_CONTAINER(frame), evbox);
   gtk_signal_connect (GTK_OBJECT(evbox), "button_press_event",
		       GTK_SIGNAL_FUNC(toggle_polar), NULL);
   gtk_widget_set_events (evbox, GDK_BUTTON_PRESS_MASK);
   gtk_widget_show (evbox);
   ui.sbar6 = gtk_drawing_area_new ();
   gtk_drawing_area_size (GTK_DRAWING_AREA(ui.sbar6), 45, 15);
   gtk_container_add (GTK_CONTAINER(evbox), ui.sbar6);
   gtk_widget_show (ui.sbar6);
   /* ortho status bar */
   frame = gtk_frame_new (NULL);
   gtk_container_border_width (GTK_CONTAINER(frame), 0);
   gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_IN);
   gtk_box_pack_start (GTK_BOX(hbox), frame, FALSE, FALSE, 2);
   gtk_widget_show (frame);
   evbox = gtk_event_box_new();
   gtk_container_add (GTK_CONTAINER(frame), evbox);
   gtk_signal_connect (GTK_OBJECT(evbox), "button_press_event",
		       GTK_SIGNAL_FUNC(toggle_ortho), NULL);
   gtk_widget_set_events (evbox, GDK_BUTTON_PRESS_MASK);
   gtk_widget_show (evbox);
   ui.sbar2 = gtk_drawing_area_new ();
   gtk_drawing_area_size (GTK_DRAWING_AREA(ui.sbar2), 45, 15);
   gtk_container_add (GTK_CONTAINER(evbox), ui.sbar2);
   gtk_widget_show (ui.sbar2);
   /* snap status bar */
   frame = gtk_frame_new (NULL);
   gtk_container_border_width (GTK_CONTAINER(frame), 0);
   gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_IN);
   gtk_box_pack_start (GTK_BOX(hbox), frame, FALSE, FALSE, 2);
   gtk_widget_show (frame);
   evbox = gtk_event_box_new();
   gtk_container_add (GTK_CONTAINER(frame), evbox);
   gtk_signal_connect (GTK_OBJECT(evbox), "button_press_event",
		       GTK_SIGNAL_FUNC(toggle_snap), NULL);
   gtk_widget_set_events (evbox, GDK_BUTTON_PRESS_MASK);
   gtk_widget_show (evbox);
   ui.sbar3 = gtk_drawing_area_new ();
   gtk_drawing_area_size (GTK_DRAWING_AREA(ui.sbar3), 40, 15);
   gtk_container_add (GTK_CONTAINER(evbox), ui.sbar3);
   gtk_widget_show (ui.sbar3);
   /* grid status bar */   
   frame = gtk_frame_new (NULL);
   gtk_container_border_width (GTK_CONTAINER(frame), 0);
   gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_IN);
   gtk_box_pack_start (GTK_BOX(hbox), frame, FALSE, FALSE, 2);
   gtk_widget_show (frame);
   evbox = gtk_event_box_new();
   gtk_container_add (GTK_CONTAINER(frame), evbox);
   gtk_signal_connect (GTK_OBJECT(evbox), "button_press_event",
		       GTK_SIGNAL_FUNC(toggle_grid_cb), NULL);
   gtk_widget_set_events (evbox, GDK_BUTTON_PRESS_MASK);
   gtk_widget_show (evbox);
   ui.sbar4 = gtk_drawing_area_new ();
   gtk_drawing_area_size (GTK_DRAWING_AREA(ui.sbar4), 40, 15);
   gtk_container_add (GTK_CONTAINER(evbox), ui.sbar4);
   gtk_widget_show (ui.sbar4);

   /* rectangle / circle status bar */
   ui.frame = gtk_frame_new (NULL);
   gtk_container_border_width (GTK_CONTAINER(ui.frame), 0);
   gtk_frame_set_shadow_type (GTK_FRAME(ui.frame), GTK_SHADOW_IN);
   gtk_widget_set_usize (ui.frame, 250, 15);
   gtk_box_pack_end (GTK_BOX(hbox), ui.frame, FALSE, FALSE, 0);
   ui.sbar5 = gtk_label_new ("");
   gtk_container_add (GTK_CONTAINER(ui.frame), ui.sbar5);
   gtk_widget_show (ui.sbar5);
   style = gtk_style_new();
   style->font = gdk_font_load ("-misc-fixed-bold-r-normal--*-100-*-*-*-*-*-*");
   gdk_font_ref (style->font);
   gtk_widget_set_style (ui.sbar5, style);

 		       
   create_main_menu();
   create_main_toolbar();

   gtk_widget_show (app_box);
   gtk_widget_show (app);

   /* Maximize application */
   gnome_win_hints_set_state (app,
                              WinStateMaximizedHoriz | WinStateMaximizedVert,
			      WinStateMaximizedHoriz | WinStateMaximizedVert);


   return;
}


/**************************************
  Update the GUI
 **************************************/
void update_ui()
{
   GList     *layers_list, *layers_name = NULL;
   layer_t   *l;
   gchar    buf[255];
   gint	    i;
   GtkStyle *style;

   ui.mode = UI_NORMAL;
   ui.kbd = KBD_COMMAND;
   change_cursor (CURSOR_NORMAL);

   /* Window name */
   g_snprintf (buf, sizeof(buf), "%s - <%s>", PACKAGE, drw.name);
   gtk_window_set_title (&GNOME_APP(app)->parent_object, buf);

   /* Recalculate zoom */
   recalculate_zoom();
   
   /* Update rulers range */
   gtk_ruler_set_range (GTK_RULER(ui.hrule), drw.win_x1, drw.win_x2, 0, drw.win_x2);
   gtk_ruler_set_range (GTK_RULER(ui.vrule), drw.win_y2, drw.win_y1, 0, drw.win_y2);

   /* Update scroll bars range */
   change_gl_hscr();
   change_gl_vscr();

   /* Show initial message in cmd_list*/
   cmd_list_insert_text (APPNAME, TRUE);
   cmd_list_insert_text ("\n", TRUE);
   for (i = 0; i < strlen(APPNAME); i++)
      cmd_list_insert_text ("-", TRUE);
   cmd_list_insert_text ("\n", TRUE);
   g_snprintf (buf, sizeof(buf), _("%s Author: %s\n"), COPYRIGHT_NOTICE, AUTHOR);
   cmd_list_insert_text (buf, TRUE);

   /* Layers */
   layers_list = drw.layers;
   l = (layer_t *) layers_list->data;
   gtk_entry_set_text (GTK_ENTRY(GTK_COMBO(ui.cmb_layers)->entry), l->name);
   gtk_entry_set_editable (GTK_ENTRY(GTK_COMBO(ui.cmb_layers)->entry), FALSE);
   while (layers_list)
   {
      l = layers_list->data;
      layers_name = g_list_append (layers_name, l->name);
      layers_list = layers_list->next;
   }
   gtk_combo_set_popdown_strings (GTK_COMBO(ui.cmb_layers), layers_name);
}


/**************************************
  App expose cb
 **************************************/
gint app_expose_cb (GtkWidget *w, GdkEventExpose *ev)
{
   gchar    buf[255];

   /* Update statusbars values */
   g_snprintf (buf, sizeof(buf), " X: %6.2f  Y: %6.2f ", 0, 0);
   gtk_label_set (GTK_LABEL(ui.sbar1), buf);

   gdk_window_clear (ui.sbar6->window);
   gdk_draw_text (ui.sbar6->window, ui.sbar1->style->font,
	          ui.sbar6->style->black_gc,
	          drw.polar ? 6 : 10, 13, drw.polar ? "POLAR" : "CART ", 5);
   gdk_draw_text (ui.sbar2->window, ui.sbar1->style->font,
	          drw.ortho ? ui.sbar2->style->black_gc : ui.sbar2->style->white_gc,
	          6, 13, "ORTHO", 5);
   gdk_draw_text (ui.sbar3->window, ui.sbar1->style->font,
	          drw.snap ? ui.sbar3->style->black_gc : ui.sbar3->style->white_gc,
	          7, 13, "SNAP", 4);
   gdk_draw_text (ui.sbar4->window, ui.sbar1->style->font,
	          drw.grid ? ui.sbar4->style->black_gc : ui.sbar4->style->white_gc,
	          7, 13, "GRID", 4);
}


/**************************************
  Change cursor
 **************************************/
void change_cursor (gint cursor)
{
   GdkCursor *curs;

   curs = gdk_cursor_new (cursor);
   gdk_window_set_cursor (ui.gl->window, curs);
   gdk_cursor_destroy (curs);
}
