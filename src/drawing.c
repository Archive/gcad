/*   gcad: simple 2D CAD program for GNOME.
 *    
 *   Copyright (C) 1998 I�igo Serna <inigo@bipv02.bi.ehu.es>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include <gnome.h>

#include "global.h"


/* Defaults for drawing */
#define DRW_NAME     _("unknown")
#define DRW_WIDTH    420		/* in mms. */
#define DRW_HEIGHT   297		/* in mms. */
#define DRW_POLAR    FALSE
#define DRW_ORTHO    FALSE
#define DRW_SNAP     TRUE
#define DRW_GRID     TRUE
#define DRW_SNAP_X   1.0		/* in mms. */
#define DRW_SNAP_Y   1.0		/* in mms. */
#define DRW_SNAP_ANG 0.5		/* in deg. */
#define DRW_GRID_X   10.0		/* in mms. */
#define DRW_GRID_Y   10.0		/* in mms. */
#define DRW_ZOOM     100.0		/* in % */


/*******************************
  Types
 *******************************/


/*******************************
  Globals
 *******************************/
draw_t drw;


/*******************************
  Local Function prototypes
 *******************************/


/*******************************
 *******************************
 *******************************
  FUNCTIONS
 *******************************
 *******************************
 *******************************/

/*******************************
  Drawing Functions
 *******************************/

/* create_draw */
void create_draw()
{
   drw.name = g_strdup (DRW_NAME);
   drw.comment = g_strdup ("");
   drw.height = DRW_HEIGHT;
   drw.width = DRW_WIDTH;
   drw.polar = DRW_POLAR;
   drw.ortho = DRW_ORTHO;
   drw.snap = DRW_SNAP;
   drw.grid = DRW_GRID;
   drw.snap_x = DRW_SNAP_X;
   drw.snap_y = DRW_SNAP_Y;
   drw.snap_ang = DRW_SNAP_ANG;
   drw.grid_x = DRW_GRID_X;
   drw.grid_y = DRW_GRID_Y;
   
   /* zoom, scr_x1..., win_x1... */
   drw.zoom = DRW_ZOOM;
   drw.win_x1 = drw.p_win_x1 = drw.width * (0 - 0.05);  /* 0 - 5% */
   drw.org_x1 = drw.scr_x1 = drw.win_x1;
   drw.win_y1 = drw.p_win_y1 = drw.height * (0 - 0.05); /* 0 - 5% */
   drw.org_y1 = drw.scr_y1 = drw.win_y1;
   drw.win_x2 = drw.p_win_x2 = drw.width * (1 + 0.05);  /* LIMIT + 5% */
   drw.org_x2 = drw.scr_x2 = drw.win_x2;
   drw.win_y2 = drw.p_win_y2 = drw.height * (1 + 0.05); /* LIMIT + 5% */
   drw.org_y2 = drw.scr_y2 = drw.win_y2;

   /* Create layers */
   drw.layers = NULL;
   drw.layers_num = 0;
   layer_add (_("Default"), 230, 230, 230, 0, 1, TRUE);
/*   create_basic_layers(); */
   drw.layers = g_list_first (drw.layers);
   drw.layer_i = 0;		/* default layer */
}


/*******************************
  Layer Functions
 *******************************/

/* layer add */
void layer_add (gchar *name, gint r, gint g, gint b,
		gint line_type, gint line_anchor, gboolean visible)
{
   layer_t *l;
   
   l = g_new (layer_t, 1);
   l->name = g_strdup (name),
   l->r = r;
   l->g = g;
   l->b = b;
   l->line_type = line_type;
   l->line_anchor = line_anchor;
   l->visible = visible;
   
   drw.layers_num++;
   drw.layers = g_list_append (drw.layers, l);
}

/* layer change */
void layer_change (guint n, gchar *name, gint r, gint g, gint b,
		   gint line_type, gint line_anchor, gboolean visible)
{
   layer_t *l;
   
   if (n == 0 || n > g_list_length(drw.layers))
      return;
      
   l = (layer_t *) g_list_nth (drw.layers, n);
   l->name = g_strdup (name),
   l->r = r;
   l->g = g;
   l->b = b;
   l->line_type = line_type;
   l->line_anchor = line_anchor;
   l->visible = visible;
}

/* layer delete */
void layer_delete (guint n)
{
   layer_t *l;
   
   if (n == 0 || n > g_list_length(drw.layers))
      return;
      
   l = (layer_t *) g_list_nth (drw.layers, n);
   if (l != NULL)
   {
      g_list_remove (drw.layers, l);
      g_free (l);
      drw.layers_num--;
   }   
}

/* create basic layers */
void create_basic_layers()
{
   layer_add (_("Axis"), 150, 150, 50, 1, 1, TRUE);
   layer_add (_("Dimensions"), 20, 230, 20, 0, 1, TRUE);
   layer_add (_("Notes"), 15, 15, 230, 0, 1, TRUE);
   layer_add (_("Hatch"), 230, 50, 200, 0, 1, TRUE);
   layer_add (_("Other"), 50, 50, 230, 0, 1, TRUE);
}
